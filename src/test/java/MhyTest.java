import com.ssm.pojo.Books;
import com.ssm.service.BooksService;
import com.ssm.service.Impl.BooksServiceImpl;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

public class MhyTest {
    @Test
    public void test1(){
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
        BooksServiceImpl booksServiceImpl = applicationContext.getBean("BooksServiceImpl", BooksServiceImpl.class);
        Books book = booksServiceImpl.selectBookById(3);
        System.out.println(book);
        book.setBookName("LinuxNB");
        booksServiceImpl.updateBook(book);
    }
}
