package com.ssm.service.Impl;

import com.ssm.dao.BookMapper;
import com.ssm.pojo.Books;
import com.ssm.service.BooksService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;

public class BooksServiceImpl implements BooksService {

    //service调dao层
    private BookMapper bookMapper;
    public void setBookMapper(BookMapper bookMapper){
        this.bookMapper = bookMapper;
    }
    @Override
    public int insertBook(Books book) {
        return bookMapper.insertBook(book);
    }

    @Override
    public int deleteBookById(int id) {
        return bookMapper.deleteBookById(id);
    }

    @Override
    public int updateBook(Books book) {
        return bookMapper.updateBook(book);
    }

    @Override
    public List<Books> selectBooks() {
        return bookMapper.selectBooks();
    }

    @Override
    public Books selectBookById(int id) {
        return bookMapper.selectBookById(id);
    }
}
