package com.ssm.dao;

import com.ssm.pojo.Books;


import java.util.List;

public interface BookMapper {

    //增加一本书
    int insertBook(Books book);

    //删除一本书
    int deleteBookById(int id);

    //更新一本书
    int updateBook(Books book);

    //查询全部书籍
    List<Books> selectBooks();

    //根据id查找书籍
    Books selectBookById(int id);
}
