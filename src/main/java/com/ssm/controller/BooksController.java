package com.ssm.controller;

import com.ssm.pojo.Books;
import com.ssm.service.BooksService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;


@Controller
@RequestMapping("/book")
public class BooksController {

    @Autowired
    @Qualifier("BooksServiceImpl")
    private BooksService booksService;

    //查询全部书籍并且返回一个书籍展示
    @RequestMapping("/allBook")
    public String list(Model model){
        List<Books> books = booksService.selectBooks();
        model.addAttribute("list", books);
        return "allBook";
    }

    //跳转页面到添加书籍
    @RequestMapping("/toAddBook")
    public String toAddBook(){

        return "addBook";
    }

    //添加书籍
    @RequestMapping("/addBook")
    public String addBook(Books book){
        System.out.println("addBook了");
        booksService.insertBook(book);
        return "redirect:/book/allBook";
    }

    //删除书籍(通过id)
    @RequestMapping("/del/{bookId}")
    public String del(@PathVariable("bookId") int id){
        booksService.deleteBookById(id);
        return "redirect:/book/allBook";
    }

    //跳转到添加书籍页面
    @RequestMapping("/toUpdateBook")
    public String toUpdatePaper(int id,Model model){
        Books book = booksService.selectBookById(id);
        model.addAttribute("book",book);
        System.out.println("跳转了");

        return "update";
    }
    //修改书籍
    @RequestMapping("/updateBook")
    public String updateBook(Books book,Model model){

        System.out.println("进入修改");
        booksService.updateBook(book);
        Books books = booksService.selectBookById(book.getBookID());
        model.addAttribute("books", books);
        return "redirect:/book/allBook";
    }
}
